package fr.jaucordi.tod;

import fr.jaucordi.tod.commands.AcceptCmd;
import fr.jaucordi.tod.commands.DeclineCmd;
import fr.jaucordi.tod.commands.TodCmd;
import fr.jaucordi.tod.commands.TodMeCmd;
import fr.jaucordi.tod.listeners.PlayerListener;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main Class
 */
public class Main extends JavaPlugin{

	private static Main instance;

	/**
	 * Return the unique instance of the plugin.
	 *
	 * @return Main Plugin instance
	 */
	public static Main getInstance(){
		return instance;
	}

	@Override
	public void onEnable(){
		instance = this;

		// registerListeners();
		registerCommands();
	}

	@Override
	public void onDisable(){
		instance = null;
		TODManager.reset();
	}

	private void registerCommands(){
		getCommand("tod").setExecutor(new TodCmd());
		getCommand("todme").setExecutor(new TodMeCmd());
		getCommand("accept").setExecutor(new AcceptCmd());
		getCommand("decline").setExecutor(new DeclineCmd());

		// TODO Add HomeTable command w/ MySQL support and delay each teleportation by calculing how many blocks has been traveled last teleportation.
	}

	/**
	 * Register listeners.
	 */
	private void registerListeners(){
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}

	/**
	 * @param name Searched player's name
	 *
	 * @return Player|null
	 */
	public Player getPlayerByName(String name){
		for (Player p : getServer().getOnlinePlayers()){
			if (p.getName().equalsIgnoreCase(name))
				return p;
		}
		return null;
	}

	public String getPluginMessagePrefix(){
		String msg = ChatColor.DARK_AQUA + "[";
		msg += ChatColor.GOLD.toString() + ChatColor.BOLD + "TOD";
		msg += ChatColor.RESET.toString() + ChatColor.DARK_AQUA + "] ";

		return msg;
	}
}