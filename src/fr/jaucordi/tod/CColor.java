package fr.jaucordi.tod;

import org.bukkit.ChatColor;

public enum CColor{

	BOLD_ITALIC(ChatColor.BOLD.toString() + ChatColor.ITALIC),
	BOLD_UNDERLINE(ChatColor.BOLD.toString() + ChatColor.UNDERLINE),
	BOLD_STRIKED(ChatColor.BOLD.toString() + ChatColor.STRIKETHROUGH),
	BOLD_MAGIC(ChatColor.BOLD.toString() + ChatColor.MAGIC),

	ITALIC_UNDERLINE(ChatColor.ITALIC.toString() + ChatColor.UNDERLINE),
	ITALIC_STRIKED(ChatColor.ITALIC.toString() + ChatColor.STRIKETHROUGH),
	ITALIC_MAGIC(ChatColor.ITALIC.toString() + ChatColor.MAGIC),

	UNDERLINE_STRIKED(ChatColor.UNDERLINE.toString() + ChatColor.STRIKETHROUGH),
	UNDERLINE_MAGIC(ChatColor.UNDERLINE.toString() + ChatColor.MAGIC),

	STRIKED_MAGIC(ChatColor.STRIKETHROUGH.toString() + ChatColor.MAGIC),

	BOLD_ITALIC_UNDERLINE(CColor.BOLD_ITALIC.code + ChatColor.UNDERLINE),
	BOLD_ITALIC_STRIKED(CColor.BOLD_ITALIC.code + ChatColor.STRIKETHROUGH),
	BOLD_ITALIC_MAGIC(CColor.BOLD_ITALIC.code + ChatColor.MAGIC),

	BOLD_UNDERLINE_STRIKED(CColor.BOLD_UNDERLINE.code + ChatColor.STRIKETHROUGH),
	BOLD_UNDERLINE_MAGIC(CColor.BOLD_UNDERLINE.code + ChatColor.MAGIC),

	BOLD_STRIKED_MAGIC(CColor.BOLD_STRIKED.code + ChatColor.MAGIC),

	BOLD_ITALIC_UNDERLINE_STRIKED(CColor.BOLD_ITALIC_UNDERLINE.code + ChatColor.STRIKETHROUGH),
	BOLD_ITALIC_UNDERLINE_MAGIC(CColor.BOLD_ITALIC_UNDERLINE.code + ChatColor.MAGIC),

	BOLD_ITALIC_UNDERLINE_STRIKED_MAGIC(CColor.BOLD_ITALIC_UNDERLINE_STRIKED.code + ChatColor.MAGIC);

	private String code;

	private CColor(String code){
		this.code = code;
	}
}