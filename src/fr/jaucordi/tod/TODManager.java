package fr.jaucordi.tod;

import fr.jaucordi.tod.handlers.TeleportationDemand;
import net.minecraft.server.v1_12_R1.ChatMessageType;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import static fr.jaucordi.tod.handlers.TeleportationDemand.ASKED;

public class TODManager{

	private static TODManager instance;

	private static List<TeleportationDemand> demands;

	/**
	 * TODManager Private Constructor
	 */
	private TODManager(){
		demands = new ArrayList<>();
	}

	/**
	 * Get unique Instance of the TODManager.
	 *
	 * @return TODManager
	 */
	public static TODManager getInstance(){
		if (instance == null)
			instance = new TODManager();

		return instance;
	}

	public static boolean askTeleport(Player from, Player to){
		TeleportationDemand demand = getInstance().createDemand(from, to);
		return (demand != null);
	}

	public static boolean askTeleport(Player from, Player to, boolean fromIsAsking){
		TeleportationDemand demand = getInstance().createDemand(from, to, fromIsAsking);
		return (demand != null);
	}

	public static boolean askTeleport(Player from, Player to, int delay){
		TeleportationDemand demand = getInstance().createDemand(from, to, delay);
		return (demand != null);
	}

	public static boolean askTeleport(Player from, Player to, int delay, boolean fromIsAsking){
		TeleportationDemand demand = getInstance().createDemand(from, to, delay, fromIsAsking);
		return (demand != null);
	}

	public static boolean acceptDemand(Player asked, Player asking){
		TeleportationDemand demand = getInstance().findDemand(asking, asked);

		if (demand == null)
			return false;

		if (!((demand.getState() & ASKED) == ASKED)){
			String msgFrom = ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Vous ne pouvez pas faire ça !";
			sendActionBar(demand.getAskedPlayer(), msgFrom);

			return true;
		}

		String msg = ChatColor.AQUA + demand.getAskedPlayer().getName() + ChatColor.GREEN + " a accepté votre demande de téléportation ! :D";
		new ActionBar(demand.getAskingPlayer(), msg, 5);
		// demand.getAskingPlayer().sendMessage(msg);

		demand.accept();

		return true;
	}

	public static boolean declineDemand(Player from, Player to){
		TeleportationDemand demand = getInstance().findDemand(from, to);

		if (demand == null)
			return false;

		if (!((demand.getState() & ASKED) == ASKED)){
			String msgFrom = ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Vous ne pouvez pas faire ça !";
			sendActionBar(demand.getAskedPlayer(), msgFrom);

			return true;
		}

		String msg = ChatColor.AQUA + demand.getAskedPlayer().getName() + ChatColor.RED + " a refusé votre demande de téléportation ! :'(";
		new ActionBar(demand.getAskingPlayer(), msg, 5);

		demand.decline();

		return true;
	}

	public static void sendActionBar(Player player, String message){
		CraftPlayer p = (CraftPlayer) player;
		// if (p.getHandle().playerConnection.networkManager.getVersion() != 47) return; // Don't run if player is not on 1.8
		IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, ChatMessageType.GAME_INFO);
		p.getHandle().playerConnection.sendPacket(ppoc);
	}

	public static void reset(){
		instance = null;
	}

	private TeleportationDemand createDemand(Player from, Player to){
		return createDemand(from, to, true);
	}

	private TeleportationDemand createDemand(Player from, Player to, int delay){
		return createDemand(from, to, delay, true);
	}

	private TeleportationDemand createDemand(Player from, Player to, boolean fromIsAsking){
		TeleportationDemand demand = findDemand(from, to);

		if (demand == null || demand.isFinished())
			demand = new TeleportationDemand(from, to, fromIsAsking);

		demands.add(demand);
		demand.ask();

		return demand;
	}

	private TeleportationDemand createDemand(Player from, Player to, int delay, boolean fromIsAsking){
		TeleportationDemand demand = findDemand(from, to);

		if (demand == null || demand.isFinished())
			demand = new TeleportationDemand(from, to, delay, fromIsAsking);

		demands.add(demand);
		demand.ask();

		return demand;
	}

	@Nullable
	private TeleportationDemand findDemand(Player from, Player to){
		for (TeleportationDemand demand : demands)
			if (demand.getAskingPlayer().getName().equalsIgnoreCase(from.getName()) && demand.getAskedPlayer().getName().equalsIgnoreCase(to.getName()))
				return demand;

		return null;
	}

	private static final class ActionBar extends BukkitRunnable{

		CraftPlayer cplayer;
		Player player;
		PacketPlayOutChat packet;
		IChatBaseComponent component;
		private String message = "";
		private int remaining;

		public ActionBar(Player player, String message, int duration){
			this.player = player;
			this.message = message;
			this.remaining = duration;

			this.component = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + this.message + "\"}");
			this.packet = new PacketPlayOutChat(this.component, ChatMessageType.GAME_INFO);

			this.runTaskTimer(Main.getInstance(), 0, 20);
		}

		public CraftPlayer getCraftPlayer(){
			if (this.cplayer == null)
				this.cplayer = (CraftPlayer) this.player;

			return this.cplayer;
		}

		/**
		 * When an object implementing interface <code>Runnable</code> is used
		 * to create a thread, starting the thread causes the object's
		 * <code>run</code> method to be called in that separately executing
		 * thread.
		 * <p>
		 * The general contract of the method <code>run</code> is that it may
		 * take any action whatsoever.
		 *
		 * @see Thread#run()
		 */
		@Override
		public void run(){
			if (this.remaining-- <= 0)
				this.cancel();

			if (this.getCraftPlayer() == null)
				this.cancel();

			this.getCraftPlayer().getHandle().playerConnection.sendPacket(this.packet);
		}
	}
}