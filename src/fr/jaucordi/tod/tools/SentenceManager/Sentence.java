package fr.jaucordi.tod.tools.SentenceManager;

import java.util.List;

public class Sentence{

	private List<Word> words;
	private int nbWords = 0;
	private int cursor = 0;

	public Sentence(String sentence){
		addWords(sentence);
	}

	public Sentence(String... parts){
		for (String part : parts)
			addWords(parts);
	}

	public Sentence addWords(String sentence){
		return addWords(sentence.split(" "));
	}

	public Sentence addWords(String[] words){
		if (words.length == 0)
			return this;

		for (String part : words){
			addWord(part);
		}

		return this;
	}

	public Sentence addWords(List<String> words){
		return addWords(words.toArray(new String[0]));
	}

	public Sentence addWord(String part){
		if (part.isEmpty())
			return this;

		this.words.add(new Word(part));
		this.nbWords++;

		return this;
	}

	public String getNextLine(){
		if (this.cursor >= this.words.size())
	}
}