package fr.jaucordi.tod.tools.SentenceManager;

import fr.jaucordi.tod.tools.Chat;

public class Word{

	private String text;
	private int length = -1;
	private int width = -1;

	private boolean bold = false;

	public Word(String word){
		this.text = word;
	}

	public Word(String word, boolean bold){
		this.text = word;
		this.bold = bold;
	}

	public String getText(){
		return this.text;
	}

	public Word setText(String text){
		this.text = text;
		this.length = -1;
		this.width = -1;

		return this;
	}

	public int getLength(){
		if (this.length < 0)
			this.length = this.text.length();

		return this.length;
	}

	public int getWidth(){
		if (this.width < 0)
			this.width = Chat.getRealLength(this.text);

		return this.width;
	}

	public boolean isBold(){
		return this.bold;
	}
}