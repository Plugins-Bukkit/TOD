package fr.jaucordi.tod.tools.MySQL;

import java.sql.*;

public class Table{

	protected static Table instance;

	private static String table = null;
	private static String primaryKey = null;
	private static Connection connection = null;

	protected Table(Connection conn){
		try{
			if (!conn.isClosed())
				connection = conn;
		}catch (SQLException e){
			e.printStackTrace();
		}

		instance = this;
	}

	public static String getTableName(){
		return table;
	}

	public static String getPK(){
		return primaryKey;
	}

	public static Connection getConnection(){
		if (connection == null)
			createConnection();

		return connection;
	}

	public static PreparedStatement prepare(String sql){
		if (getConnection() == null)
			return null;

		try{
			return getConnection().prepareStatement(sql);
		}catch (SQLException e){
			System.out.println("Problème de BDD => " + e.getMessage());
		}

		return null;
	}

	private static void createConnection(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost/minecraft_134498", "minecraft_134498", "antoines");
		}catch (ClassNotFoundException e){
			System.out.println("Problème de driver => " + e.getMessage());
		}catch (SQLException e){
			System.out.println("Problème de BDD => " + e.getMessage());
		}
	}

	public static Table getInstance(){
		return instance;
	}
}