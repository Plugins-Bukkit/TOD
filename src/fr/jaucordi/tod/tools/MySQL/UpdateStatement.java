package fr.jaucordi.tod.tools.MySQL;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UpdateStatement<ENT extends Entity>{

	private Connection conn;
	private PreparedStatement stmt;
	private ResultSet res;

	private ENT entity;

	private List<Field> changedFields;

	public UpdateStatement(ENT e){
		this.entity = e;
		System.out.println("UpdateStatement => " + e.getClass().getName());
	}

	public UpdateStatement addField(Field f){
		if (!this.changedFields.contains(f))
			this.changedFields.add(f);

		return this;
	}

	public UpdateStatement addFields(List<Field> fields){
		for (Field f : fields)
			if (!this.changedFields.contains(f))
				this.changedFields.add(f);

		return this;
	}

	public boolean execute(){
		try{
			if (this.stmt.execute()){
				ResultSet res = stmt.getResultSet();
				return res.rowUpdated();
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		try{
			this.conn.close();
		}catch (SQLException e){
			e.printStackTrace();
		}

		return false;
	}
}