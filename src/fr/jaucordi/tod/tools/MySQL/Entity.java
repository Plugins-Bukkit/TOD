package fr.jaucordi.tod.tools.MySQL;

import com.sun.deploy.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Entity<TB extends Table>{

	protected Entity oldInstance;
	protected List<Field> changedFields;

	protected TB table;

	public Entity(){
		this.setOldInstance(this);
	}

	public Entity getOldInstance(){
		return this.oldInstance;
	}

	protected void setOldInstance(Entity ent){
		this.oldInstance = ent;
	}

	public List<Field> getChangedFields(){
		if (this.changedFields == null)
			this.changedFields = new ArrayList<>();

		ReflectionUtil.getClass(this.getClass().getName(), ClassLoader.getSystemClassLoader());

		Class clazz = this.getClass();
		for (Field f : clazz.getDeclaredFields()){
			try{
				if (!f.get(this).equals(f.get(this.getOldInstance())))
					this.changedFields.add(f);
			}catch (IllegalAccessException e){
				e.printStackTrace();
			}
		}

		return this.changedFields;
	}

	public TB getTable(){
		return this.table;
	}
}