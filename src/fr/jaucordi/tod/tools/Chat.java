package fr.jaucordi.tod.tools;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jaucordi on 20/06/17.
 */
public class Chat{

	private final static int CENTER_PX = 154 * 2;

	public static int getRealLength(String message){
		message = ChatColor.translateAlternateColorCodes('&', message);

		int messagePxSize = 0;
		boolean previousCode = false;
		boolean isBold = false;

		for (char c : message.toCharArray()){
			if (c == '§'){
				previousCode = true;
				continue;
			}else if (previousCode){
				previousCode = false;
				if (Character.toString(c).equalsIgnoreCase(Character.toString(ChatColor.BOLD.toString().charAt(1)))){
					isBold = true;
					continue;
				}else
					isBold = false;
			}else{
				DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
				messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
				messagePxSize++;
			}
		}

		return messagePxSize;
	}

	public static String getCharsInByPixel(String str, int px){
		String message = ChatColor.translateAlternateColorCodes('&', str);

		int messagePxSize = 0;
		boolean previousCode = false;
		boolean isBold = false;
		String part = "";

		for (char c : message.toCharArray()){
			if (c == '§'){
				previousCode = true;
				continue;
			}else if (previousCode){
				previousCode = false;
				if (Character.toString(c).equalsIgnoreCase(Character.toString(ChatColor.BOLD.toString().charAt(1)))){
					isBold = true;
					continue;
				}else
					isBold = false;
			}else{
				DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
				messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
				messagePxSize++;
				part += c;
			}

			if (messagePxSize == CENTER_PX)
				return part;
		}

		return str;
	}

	public static int getTotalMissingLength(String message){
		return getTotalMissingLength(message, CENTER_PX);
	}
	public static int getTotalMissingLength(String message, int maxWidth){
		int len = getRealLength(message);
		if (len >= maxWidth)
			return 0;
		return maxWidth - len;
	}

	public static int getMissingLength(String message){
		return getMissingLength(message, CENTER_PX);
	}
	public static int getMissingLength(String message, int maxWidth){
		return getTotalMissingLength(message, maxWidth) / 2;
	}

	public static String getSpacesToCompensate(String message){
		return getSpacesToCompensate(message, CENTER_PX);
	}
	public static String getSpacesToCompensate(String message, int maxWidth){
		int toCompensate = getMissingLength(message, maxWidth);
		int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
		int compensated = 0;
		StringBuilder sb = new StringBuilder();
		while (compensated < toCompensate){
			sb.append(" ");
			compensated += spaceLength;
		}
		sb.append(" ");
		return sb.toString();
	}

	public static int getSpacesNumberToCompensate(String message){
		return getSpacesToCompensate(message).length();
	}

	public static List<String> centerMultiple(String message){
		return centerMultiple(message, CENTER_PX);
	}
	public static List<String> centerMultiple(String message, int maxWidth){
		List<String> lines = splitIntoLines(message, maxWidth);
		List<String> resList = new ArrayList<>();

		lines.forEach(s -> {
			resList.add(s);
		});

		return resList;
	}

	public static List<String> centerMultiple(List<String> lines){
		return centerMultiple(lines, CENTER_PX);
	}
	public static List<String> centerMultiple(List<String> lines, int maxWidth){
		List<String> resList = new ArrayList<>();

		lines.forEach(s -> {
			resList.addAll(centerMultiple(s));
		});

		return resList;
	}

	public static String centerSingle(String message){
		return centerSingle(message, CENTER_PX);
	}

	public static String centerSingle(String message, int maxWidth){
		String spaces = getSpacesToCompensate(message, maxWidth);
		int spacesLen = spaces.length();
		int spacesRealLen = getRealLength(spaces);
		int msgLen = ChatColor.stripColor(message).length();
		int msgRealLen = getRealLength(message);

		System.out.println("(" + msgLen + ") (" + msgRealLen + "px <= " + maxWidth + "px) " + message);
		System.out.println("(" + spacesLen + ") (" + spacesRealLen + "px + " + msgRealLen + "px) = " + (spacesRealLen + msgRealLen) + "px");
		return spaces + message;
	}

	public static List<String> splitIntoLines(String message){
		return splitIntoLines(message, CENTER_PX);
	}

	public static List<String> splitIntoLines(String message, int maxWidth){
		List<String> resList = new ArrayList<>();

		if (getRealLength(message) > maxWidth){
			String msg = message;
			String lastLine = "";
			String lastColor = "";
			do{
				int messageLen = msg.length();
				int msgLen = getRealLength(msg);

				String line;
				if (msgLen > maxWidth)
					line = lastColor + getCharsInByPixel(msg, maxWidth);
				else
					line = lastColor + msg;

				lastLine = line;

				resList.add(centerSingle(line, maxWidth));
				lastLine = line;
				lastColor = ChatColor.getLastColors(lastLine);

				if (msgLen > maxWidth)
					msg = msg.substring(ChatColor.stripColor(line).length());
				else
					msg = "";
			}while (!msg.isEmpty());
		}else{
			String spaces = getSpacesToCompensate(message);
			resList.add(spaces + message);
		}

		return resList;
	}

	public static List<String> center(String str, int maxWidth){
		List<String> res = new ArrayList<>();

		if (getRealLength(str) <= maxWidth){
			res.add(str);
			return res;
		}

		String lastColor = "";
		String lastLine = "";

		while (!str.isEmpty()){
			int len = getRealLength(str);
			int limit = maxWidth;
			if (len <= maxWidth)
				limit = len;

			String line = str.substring(0, limit);
			if (line.charAt(line.length() - 1) == ChatColor.BOLD.toString().charAt(0))
				line = line.substring(0, line.length() - 1);

			System.out.println(line);

			lastColor = ChatColor.getLastColors(lastLine);
			lastLine = lastColor + line;
			str = str.replace(line, "");

			res.add(getSpacesToCompensate(line) + line);
		}

		return res;
	}
}