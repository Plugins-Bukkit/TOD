package fr.jaucordi.tod.tools.HomeManager.Entity;

import fr.jaucordi.tod.tools.HomeManager.Table.PlayerTable;
import fr.jaucordi.tod.tools.MySQL.Entity;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerEntity extends Entity{

	private int id_player = 0;
	private String name_player;
	private Player player;

	private PlayerEntity oldInstance;

	public PlayerEntity(int id, String name){
		this.setId(id);
		this.setName(name, false);
	}

	public PlayerEntity setId(int id){
		this.id_player = id;

		return this;
	}
	public PlayerEntity setName(String name){
		return this.setName(name, true);
	}
	public PlayerEntity setName(String name, boolean updateDB){
		if (!name.isEmpty())
			this.name_player = name;

		if (updateDB)
			if (!PlayerTable.update(this))
				System.out.println("Failed to update player's name in database!");

		return this;
	}

	public boolean update(){
		return PlayerTable.update(this);
	}
}