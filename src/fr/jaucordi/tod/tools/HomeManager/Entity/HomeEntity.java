package fr.jaucordi.tod.tools.HomeManager.Entity;

import fr.jaucordi.tod.Main;
import fr.jaucordi.tod.tools.HomeManager.Table.PlayerTable;
import fr.jaucordi.tod.tools.HomeManager.Table.WorldTable;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class HomeEntity{

	private int id_home = 0;
	private int player_id_player;
	private int world_id_world;
	private String name_home;
	private double locX_home;
	private double locY_home;
	private double locZ_home;
	private float locYaw_home;
	private float locPitch_home;

	private Location location;

	public int getId(){
		return id_home;
	}

	public int getOwnerId(){
		return this.player_id_player;
	}

	public Player getOwner(){
		Player p = PlayerTable.find(this.getOwnerId());
	}

	public String getName(){
		return this.name_home;
	}

	public double getX(){
		return this.locX_home;
	}

	public double getY(){
		return this.locY_home;
	}

	public double getZ(){
		return this.locZ_home;
	}

	public float getYaw(){
		return this.locYaw_home;
	}

	public float getPitch(){
		return this.locPitch_home;
	}

	public Location getLocation(){
		if (this.location == null)
			this.location = new Location(getWorld(), getX(), getY(), getZ(), getYaw(), getPitch());

		return this.location;
	}

	public World getWorld(){
		World w = WorldTable.getById(this.getWorldId());
		if (w == null)
			return Main.getInstance().getServer().getWorlds().get(0);

		return w;
	}

	public int getWorldId(){
		return world_id_world;
	}

	public boolean setWorldId(int world_id){
		if (world_id == this.getWorldId())
			return true;

		return this.update("world_id_world", world_id);
	}
}