package fr.jaucordi.tod.tools.HomeManager.Table;

import fr.jaucordi.tod.Main;
import fr.jaucordi.tod.tools.MySQL.Table;
import org.bukkit.World;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WorldTable extends Table{

	protected static String table = "world";
	protected static String primaryKey = "id_world";

	public static World getById(int id){
		Connection conn = getConnection();

		try{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM " + getTableName() + " WHERE " + getPK() + "=?");
			stmt.setInt(1, id);
			if (!stmt.execute())
				return null;

			ResultSet res = stmt.getResultSet();
			if (res == null)
				return null;

			if (!res.next())
				return null;

			World w = Main.getInstance().getServer().getWorld(res.getString("name_world"));
			if (w == null)
				return null;

			return w;
		}catch (SQLException e){
			e.printStackTrace();
		}

		return null;
	}

	public static World getByName(String name){
		Connection conn = getConnection();
		World world = null;

		try{
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `" + getTableName() + "` WHERE `name_world`=?");
			stmt.setString(1, name);
			if (!stmt.execute())
				return null;

			ResultSet res = stmt.getResultSet();
			if (res == null || !res.next())
				return null;

			World w = Main.getInstance().getServer().getWorld(res.getString("name_world"));
			if (w == null)
				return null;

			world = w;
		}catch (SQLException e){
			e.printStackTrace();
		}

		return world;
	}
}