package fr.jaucordi.tod.tools.HomeManager.Table;

import fr.jaucordi.tod.Main;
import fr.jaucordi.tod.tools.MySQL.Table;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class HomeTable extends Table{

	protected static String table = "home";

	public HomeTable(int player_id_player, String name_home, double locX_home, double locY_home, double locZ_home, float locYaw_home, float locPitch_home){
		this.player_id_player = player_id_player;
		this.name_home = name_home;
		this.locX_home = locX_home;
		this.locY_home = locY_home;
		this.locZ_home = locZ_home;
		this.locYaw_home = locYaw_home;
		this.locPitch_home = locPitch_home;

		this.insert();
	}

	public boolean save(){
		if (this.id_home > 0){

		}else{
			return this.insert();
			// "INSERT INTO `home` VALUES()"
		}

		return false;
	}

	public boolean insert(){
		try{
			return !(getConnection() == null || getConnection().isClosed()) && this.insert(prepare("INSERT INTO `" + getTableName() + "`(`player_id_player`, `world_id_world`, `name_home`, `locX_home`, `locY_home`, `locZ_home`, `locYaw_home`, `locPitch_home`) VALUES(?, ?, ?, ?, ?, ?, ?, ?)"));
		}catch (SQLException e){
			e.printStackTrace();
		}

		return false;
	}
	private boolean insert(PreparedStatement stmt){
		try{
			if (!stmt.execute())
				return false;

			stmt.getConnection().close();

			return stmt.getResultSet().rowInserted();
		}catch (SQLException e){
			e.printStackTrace();
		}

		return false;
	}

	public boolean update(String name, String value){
		PreparedStatement stmt = Table.prepare("UPDATE `" + getTableName() + "` SET " + name + "=?");
		if (stmt == null)
			return false;

		try{
			stmt.setString(1, value);

			return this.update(stmt);
		}catch (SQLException e){
			e.printStackTrace();
		}

		return false;
	}
	private boolean update(PreparedStatement stmt){
		try{
			boolean update = (stmt.executeUpdate() > 0);
			stmt.getConnection().close();

			return update;
		}catch (SQLException e){
			e.printStackTrace();
		}

		return false;
	}

	public boolean delete(){
		try{
			return !(getConnection() == null || getConnection().isClosed()) && this.delete(prepare("DELETE FROM `" + getTableName() + "` WHERE `" + getPK() +"`=?"));
		}catch (SQLException e){
			e.printStackTrace();
		}

		return false;
	}
	public boolean delete(PreparedStatement stmt){
		try{
			stmt.setInt(1, this.getId());
			return stmt.execute();
		}catch (SQLException e){
			e.printStackTrace();
		}

		return false;
	}
}