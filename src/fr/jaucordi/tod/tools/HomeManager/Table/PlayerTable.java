package fr.jaucordi.tod.tools.HomeManager.Table;

import fr.jaucordi.tod.tools.HomeManager.Entity.PlayerEntity;
import fr.jaucordi.tod.tools.MySQL.Table;
import fr.jaucordi.tod.tools.MySQL.UpdateStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerTable extends Table{

	protected static String table = "player";
	protected static String primaryKey = "id_player";

	protected PlayerTable(Connection conn){
		super(conn);
	}

	public static PlayerEntity getPlayer(int id){
		try{
			PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM `player` WHERE `id_player`=?");
			stmt.setInt(1, id);
			if (stmt.execute()){
				ResultSet res = stmt.getResultSet();
				if (res != null && res.next()){
					PlayerEntity pEnt = new PlayerEntity(res.getInt("id_player"), res.getString("name_player"));
					getConnection().close();
					return pEnt;
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			try{
				getConnection().close();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		return null;
	}

	public static PlayerEntity getPlayer(String name){
		try{
			PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM `player` WHERE `name_player`=?");
			stmt.setString(1, name);
			if (stmt.execute()){
				ResultSet res = stmt.getResultSet();
				if (res != null && res.next()){
					PlayerEntity pEnt = new PlayerEntity(res.getInt("id_player"), res.getString("name_player"));
					getConnection().close();
					return pEnt;
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			try{
				getConnection().close();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		return null;
	}

	public static boolean update(PlayerEntity p){
		UpdateStatement<PlayerEntity> stmt = new UpdateStatement<>(p);
		stmt.addFields(p.getChangedFields());

		return stmt.execute();
	}
}