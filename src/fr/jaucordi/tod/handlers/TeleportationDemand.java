package fr.jaucordi.tod.handlers;

import com.bobacadodl.JSONChatLib.*;
import fr.jaucordi.tod.Main;
import fr.jaucordi.tod.TODManager;
import fr.jaucordi.tod.tools.Chat;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class TeleportationDemand{

	public final static int NON_STARTED = 1;
	public final static int ASKED = 2;
	public final static int ACCEPTED = 4;
	public final static int DECLINED = 8;
	public final static int CANCELED = 16;
	public final static int IN_COUNTDOWN = 32;
	public final static int FINISHED = 64;

	private Player from;
	private Player to;

	private Location toLocation;

	private boolean fromIsAsking = true;
	private int state = NON_STARTED;
	private int delay = 5;

	private CountDown countDown;

	/**
	 * Create a new Teleportation Demand.
	 *
	 * @param from Player who teleports to the other
	 * @param to   Player where the other one teleports to.
	 */
	public TeleportationDemand(Player from, Player to){
		this.from = from;
		this.to = to;
	}

	/**
	 * Create a new Teleportation Demand.
	 *
	 * @param from         Player who teleports to the other
	 * @param to           Player where the other one teleports to.
	 * @param fromIsAsking <b>TRUE</b> if Player <b>from</b> is asking the <b>TOD</b>, <b>FALSE</b> otherwise
	 */
	public TeleportationDemand(Player from, Player to, boolean fromIsAsking){
		this.from = from;
		this.to = to;
		this.fromIsAsking = fromIsAsking;
	}

	public TeleportationDemand(Player from, Player to, int delay){
		this.from = from;
		this.to = to;
		this.setDelay(delay);
	}

	public TeleportationDemand(Player from, Player to, int delay, boolean fromIsAsking){
		this.from = from;
		this.to = to;
		this.fromIsAsking = fromIsAsking;
		this.setDelay(delay);
	}

	public void setDelay(int delay){
		this.delay = delay;

		if (!this.isDelayed()){
			this.countDown = new CountDown(this.delay);
		}else{
			boolean wasStarted = (this.countDown.task != null);
			if (wasStarted)
				this.countDown.cancel();
			this.countDown.setTimer(this.delay);
			if (wasStarted)
				this.countDown.start();
		}
	}

	public boolean isDelayed(){
		return (this.countDown != null);
	}

	public Player getFrom(){
		return this.from;
	}

	public Player getTo(){
		return this.to;
	}

	/**
	 * Perform player FROM asking to player TO for teleport on him.
	 */
	public void ask(){
		getAskedPlayer().sendMessage("");
		getAskedPlayer().sendMessage(getMessageHeader());
		getAskedPlayer().sendMessage("");
		if (fromIsAsking)
			if (this.isDelayed())
				getAskedPlayer().sendMessage(Chat.centerMultiple(ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.YELLOW + " souhaiterait se téléporter sur vous dans " + this.countDown.timer + " seconde" + ((this.countDown.timer > 1) ? "s" : "") + " !").toArray(new String[0]));
			else
				getAskedPlayer().sendMessage(Chat.centerMultiple(ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.YELLOW + " souhaiterait se téléporter sur vous !").toArray(new String[0]));
		else if (this.isDelayed())
			getAskedPlayer().sendMessage(Chat.centerMultiple(ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.YELLOW + " souhaiterait vous téléporter sur lui dans " + this.countDown.timer + " seconde" + ((this.countDown.timer > 1) ? "s" : "") + " !").toArray(new String[0]));
		else
			getAskedPlayer().sendMessage(Chat.centerMultiple(ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.YELLOW + " souhaiterait vous téléporter sur lui !").toArray(new String[0]));

		getAskedPlayer().sendMessage("");

		String msg = ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "-=[ " + ChatColor.RESET + ChatColor.GREEN + "ACCEPTER" + ChatColor.DARK_AQUA + ChatColor.BOLD + " ]=-      -=[ " + ChatColor.RESET + ChatColor.RED + "REFUSER" + ChatColor.DARK_AQUA + ChatColor.BOLD + " ]=-";
		JSONChatMessage message = new JSONChatMessage(Chat.getSpacesToCompensate(msg), null, null);

		JSONChatExtra openAccept = new JSONChatExtra("-=[ ", JSONChatColor.DARK_AQUA, Collections.singletonList(JSONChatFormat.BOLD));
		JSONChatExtra accept = new JSONChatExtra("ACCEPTER", JSONChatColor.GREEN, null);
		JSONChatExtra closeAccept = new JSONChatExtra(" ]=-", JSONChatColor.DARK_AQUA, Collections.singletonList(JSONChatFormat.BOLD));

		ItemStack item = new ItemStack(Material.BARRIER, 1);
		ItemMeta metas = item.getItemMeta();

		ArrayList<String> lores = new ArrayList<>();
		lores.add("");
		lores.add(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "ATTENTION !");
		if (fromIsAsking)
			lores.add(ChatColor.DARK_RED + "Cliquer ici téléportera " + ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.DARK_RED + " sur vous !");
		else
			lores.add(ChatColor.DARK_RED + "Cliquer ici vous téléportera pas sur  " + ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.DARK_RED + " !");


		List<String> allLines = (List<String>) lores.clone();
		allLines.add(ChatColor.GOLD.toString() + ChatColor.BOLD + ("Cliquer pour accepter !").toUpperCase(Locale.FRANCE));

		metas.setLore(Chat.centerMultiple(lores));
		metas.setDisplayName(Chat.centerSingle(ChatColor.GOLD.toString() + ChatColor.BOLD + ("Cliquer pour accepter !").toUpperCase(Locale.FRANCE)));
		item.setItemMeta(metas);

		accept.setHoverEvent(JSONChatHoverEventType.SHOW_ITEM, item);
		accept.setClickEvent(JSONChatClickEventType.RUN_COMMAND, "/accept " + from.getName());
		message.addExtra(openAccept);
		message.addExtra(accept);
		message.addExtra(closeAccept);

		JSONChatExtra openDecline = new JSONChatExtra("      -=[ ", JSONChatColor.DARK_AQUA, Collections.singletonList(JSONChatFormat.BOLD));
		JSONChatExtra decline = new JSONChatExtra("REFUSER", JSONChatColor.RED, null);
		JSONChatExtra closeDecline = new JSONChatExtra(" ]=-", JSONChatColor.DARK_AQUA, Collections.singletonList(JSONChatFormat.BOLD));

		item = new ItemStack(Material.BARRIER, 1);
		metas = item.getItemMeta();

		lores = new ArrayList<>();
		lores.add("");
		lores.add(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "ATTENTION !");
		if (fromIsAsking)
			lores.add(ChatColor.DARK_RED + "Cliquer ici ne téléportera pas " + ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.DARK_RED + " sur vous !");
		else
			lores.add(ChatColor.DARK_RED + "Cliquer ici ne vous téléportera pas sur " + ChatColor.AQUA + getAskingPlayer().getName() + ChatColor.DARK_RED + " !");

		allLines = (List<String>) lores.clone();
		allLines.add(ChatColor.GOLD.toString() + ChatColor.BOLD + ("Cliquer pour refuser !").toUpperCase(Locale.FRANCE));

		metas.setLore(Chat.centerMultiple(lores));
		metas.setDisplayName(Chat.centerSingle(ChatColor.GOLD.toString() + ChatColor.BOLD + ("Cliquer pour refuser !").toUpperCase(Locale.FRANCE)));
		item.setItemMeta(metas);

		decline.setHoverEvent(JSONChatHoverEventType.SHOW_ITEM, item);
		decline.setClickEvent(JSONChatClickEventType.RUN_COMMAND, "/decline " + from.getName());
		message.addExtra(openDecline);
		message.addExtra(decline);
		message.addExtra(closeDecline);

		message.sendToPlayer(getAskedPlayer());

		getAskedPlayer().sendMessage("");
		state = ASKED;
	}

	private Location getLocation(){
		return this.toLocation;
	}

	/**
	 * Accept the teleportation demand et teleport player FROM to player TO.
	 */
	public void accept(){
		if (this.isDelayed()){
			this.state = IN_COUNTDOWN;
			this.toLocation = getAskedPlayer().getEyeLocation();
			this.countDown.start();
		}else{
			this.state = ACCEPTED;
			teleport();
		}
	}

	public boolean isFinished(){
		return (getState() == ACCEPTED || getState() == DECLINED || getState() == FINISHED);
	}

	/**
	 * Decline the teleportation demand.
	 */
	public void decline(){
		state = DECLINED;
	}

	/**
	 * Execute the teleport procedure.
	 */
	private void teleport(){
		if (getLocation() != null){
			this.getAskingPlayer().teleport(this.getLocation().add(0f, -0.5f, 0f));
			this.getLocation().getWorld().spawnParticle(Particle.PORTAL, this.getLocation().add(0f, 0f, 0f), 5);
			getAskedPlayer().spawnParticle(Particle.VILLAGER_HAPPY, getAskedPlayer().getLocation().add(0f, 2.5f, 0f), 1);
			TODManager.sendActionBar(this.getAskedPlayer(), ChatColor.AQUA.toString() + ChatColor.BOLD + this.getAskingPlayer().getName() + ChatColor.GREEN + " est bien arrivé ! =D");
			this.state = FINISHED;
		}else{
			getAskingPlayer().teleport(getAskedPlayer());
			getAskedPlayer().spawnParticle(Particle.VILLAGER_HAPPY, getAskedPlayer().getLocation().add(0f, 2.5f, 0f), 1);
			TODManager.sendActionBar(this.getAskedPlayer(), ChatColor.AQUA.toString() + ChatColor.BOLD + this.getAskingPlayer().getName() + ChatColor.GREEN + " est bien arrivé ! =D");
			this.state = ACCEPTED;
		}
	}

	/**
	 * Cancel the teleportation demand.
	 */
	public void cancel(){
		state = CANCELED;

		String msgFrom = "Demande de téléportation sur " + ChatColor.BLUE + getAskedPlayer().getName() + ChatColor.AQUA + " annulée.";
		getAskingPlayer().sendMessage(Main.getInstance().getPluginMessagePrefix() + msgFrom);
	}

	/**
	 * Get the player who ask for teleport the other one.
	 *
	 * @return Player
	 */
	public Player getAskingPlayer(){
		if (fromIsAsking)
			return from;
		else
			return to;
	}

	/**
	 * Get the player who receive the other one.
	 *
	 * @return Player
	 */
	public Player getAskedPlayer(){
		if (fromIsAsking)
			return to;
		else
			return from;
	}

	/**
	 * Return the TOD Information header.
	 *
	 * @return String
	 */
	private String getMessageHeader(){
		return ChatColor.GOLD.toString() + "------------------- TOD Information -------------------";
	}

	/**
	 * Return the actual state of the Teleportation Demand
	 *
	 * @return int
	 */
	public int getState(){
		return state;
	}

	public void setState(int state){
		this.state = state;
	}

	public TeleportationDemand setFromIsAsking(boolean fromIsFrom){
		this.fromIsAsking = fromIsFrom;

		return this;
	}

	private final class CountDown implements Runnable{

		private BukkitTask task;
		private int timer = 5;

		private String lastColor = ChatColor.GREEN.toString();

		CountDown(int delay){
			this.timer = delay;
		}

		void setTimer(int timer){
			boolean wasStarted = (this.task != null);
			if (wasStarted)
				this.cancel();
			this.timer = timer;
			this.start();
		}

		void start(){
			this.task = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), this, 0, 20);
		}

		void cancel(){
			if (this.task != null){
				Bukkit.getScheduler().cancelTask(this.task.getTaskId());
				this.task = null;
			}
		}

		private void announceTimeRemaining(){
			if (this.timer == 0){

			}
			if (this.timer > 0 && this.timer <= 5){
				String msg = ChatColor.AQUA.toString() + ChatColor.BOLD + TeleportationDemand.this.from.getName();

				if (this.lastColor.equals(ChatColor.RED.toString())){
					msg += ChatColor.GREEN + " incoming in " + ChatColor.BOLD + this.timer + ChatColor.RESET + ChatColor.GREEN + " ! =D";
					this.lastColor = ChatColor.GREEN.toString();
				}else{
					msg += ChatColor.RED + " incoming in " + ChatColor.BOLD + this.timer + ChatColor.RESET + ChatColor.RED + " ! =D";
					this.lastColor = ChatColor.RED.toString();
				}

				TODManager.sendActionBar(getAskedPlayer(), msg);
				getAskingPlayer().sendTitle(ChatColor.AQUA + getAskedPlayer().getName(), ChatColor.RED.toString() + ChatColor.BOLD + String.format(Locale.FRANCE, "Téléportation dans %1$d", this.timer), 0, (20 * 2), 10);
			}

			if (this.timer > 5 && this.timer <= 30 && (this.timer % 10) == 0)
				getAskingPlayer().sendTitle(ChatColor.AQUA + getAskedPlayer().getName(), ChatColor.RED.toString() + ChatColor.BOLD + String.format(Locale.FRANCE, "Téléportation dans %1$d", this.timer), 0, (20 * 2), 10);

			if (this.timer == 60)
				getAskingPlayer().sendTitle(ChatColor.AQUA + getAskedPlayer().getName(), ChatColor.RED.toString() + ChatColor.BOLD + String.format(Locale.FRANCE, "Téléportation dans %1$d", this.timer), 0, (20 * 2), 10);
		}

		/**
		 * When an object implementing interface <code>Runnable</code> is used
		 * to create a thread, starting the thread causes the object's
		 * <code>run</code> method to be called in that separately executing
		 * thread.
		 * <p>
		 * The general contract of the method <code>run</code> is that it may
		 * take any action whatsoever.
		 *
		 * @see Thread#run()
		 */
		@Override
		public void run(){
			if (TeleportationDemand.this.getState() != IN_COUNTDOWN){
				this.cancel();
				return;
			}

			if (this.timer == 0)
				if (TeleportationDemand.this.getState() == IN_COUNTDOWN)
					TeleportationDemand.this.teleport();

			announceTimeRemaining();

			if (this.timer <= 0)
				this.cancel();
			else
				timer--;
		}
	}
}