package fr.jaucordi.tod.commands;

import fr.jaucordi.tod.Main;
import fr.jaucordi.tod.TODManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TodMeCmd implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings){
		if (!(commandSender instanceof Player))
			return false;

		Player player = (Player) commandSender;
		Player target;

		if (strings.length < 1 || strings.length > 2)
			return false;

		target = Main.getInstance().getPlayerByName(strings[0]);
		if (target == null)
			player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Le joueur doit être connecté.");
		else{
			if (strings.length >= 2){
				int timer = 0;
				try{
					timer = Integer.parseInt(strings[1]);
				}catch (NumberFormatException e){
					e.printStackTrace();
					player.sendMessage(ChatColor.RED.toString() + ChatColor.ITALIC + "Je n'ai pas compris ce que vous essayez de me dire, vous devez me donner un nombre de secondes à écouler avant de vous téléporter à cet endroit : ");
					player.sendMessage(ChatColor.RED.toString() + ChatColor.ITALIC + "/" + s + " " + strings[0] + ChatColor.BOLD + " >>> " + strings[1] + " <<<");
					return true;
				}

				if (timer >= 1 && timer <= 60){
					if (!TODManager.askTeleport(player, target, timer, false))
						player.sendMessage(ChatColor.DARK_RED + "Une demande de téléportation existe déjà entre vous et " + ChatColor.AQUA + target.getName() + ChatColor.DARK_RED + " !");
				}else{
					player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Vous pouvez programmer une téléportation jusqu'à 1 minute à l'avance. (" + timer + ")");
					player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Si vous souhaitez faire une demande de téléportation instantanée, ajoutez uniquement le pseudo du joueur ciblé après le nom de la commande.");
				}
			}else{
				if (!TODManager.askTeleport(target, player, false))
					player.sendMessage(ChatColor.DARK_RED + "Une demande de téléportation existe déjà entre vous et " + ChatColor.AQUA + target.getName() + ChatColor.DARK_RED + " !");
			}
		}

		return true;
	}
}