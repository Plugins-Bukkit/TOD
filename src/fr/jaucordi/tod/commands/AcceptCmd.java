package fr.jaucordi.tod.commands;

import fr.jaucordi.tod.Main;
import fr.jaucordi.tod.TODManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class AcceptCmd implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings){
		if (!(commandSender instanceof Player))
			return false;

		if (strings.length != 1)
			return false;

		Player player = (Player) commandSender;
		Player target;

		target = Main.getInstance().getPlayerByName(strings[0]);
		if (target == null)
			player.sendMessage(ChatColor.RED + "Le joueur doit être connecté.");
		else if (!TODManager.acceptDemand(player, target))
			player.sendMessage(Main.getInstance().getPluginMessagePrefix() + ChatColor.RED + "Demande de téléportation introuvable.");

		return true;
	}
}