package com.bobacadodl.JSONChatLib;

import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * User: bobacadodl
 * Date: 10/27/13
 * Time: 9:03 PM
 */
public class JSONChatExtra{
	private JSONObject chatExtra;

	public JSONChatExtra(String text, @Nullable JSONChatColor color, @Nullable List<JSONChatFormat> formats){
		chatExtra = new JSONObject();
		chatExtra.put("text", text);
		if (color != null)
			chatExtra.put("color", color.getColorString());
		if (formats != null){
			for (JSONChatFormat format : formats){
				chatExtra.put(format.getFormatString(), true);
			}
		}
	}

	public static String capitalize(String string){
		String[] NameList = string.toLowerCase().replace("_", " ").replace("-", " ").split(" ");
		String Name = "";

		for (String Word : NameList){
			Name += Word.substring(0, 1).toUpperCase() + Word.substring(1).toLowerCase() + " ";
		}

		if (Name.endsWith(" "))
			Name = Name.substring(0, Name.length() - 1);
		return Name;
	}

	public void setHoverEvent(JSONChatHoverEventType action, ItemStack itemStack){
		JSONObject hoverEvent = new JSONObject();
		hoverEvent.put("action", action.getTypeString());

		String name = "" + (itemStack.getItemMeta().getDisplayName() != null ? itemStack.getItemMeta().getDisplayName() : capitalize(itemStack.getType().toString()));
		String value = "{id:" + itemStack.getType().name() + ",Damage:" + itemStack.getDurability() + ",Count:" + itemStack.getAmount() + ",tag:{display:{Name:\"" + name + "\"";

		if (itemStack.getItemMeta().hasLore()){
			value = value + ", Lore:[";

			for (String lore : itemStack.getItemMeta().getLore()){
				value = value + (itemStack.getItemMeta().getLore().size() == 1 || itemStack.getItemMeta().getLore().get(itemStack.getItemMeta().getLore().size() - 1) == lore ? ("\"" + lore + "\"") : ("\"" + lore + "\", "));
			}

			value = value + "]";
		}

		value = value + "}}}";

		hoverEvent.put("value", value);

		chatExtra.put("hoverEvent", hoverEvent);
	}

	public void setClickEvent(JSONChatClickEventType action, String value){
		JSONObject clickEvent = new JSONObject();
		clickEvent.put("action", action.getTypeString());
		clickEvent.put("value", value);
		chatExtra.put("clickEvent", clickEvent);
	}

	public void setHoverEvent(JSONChatHoverEventType action, String value){
		JSONObject hoverEvent = new JSONObject();
		hoverEvent.put("action", action.getTypeString());
		hoverEvent.put("value", value);
		chatExtra.put("hoverEvent", hoverEvent);
	}

	public JSONObject toJSON(){
		return chatExtra;
	}
}
